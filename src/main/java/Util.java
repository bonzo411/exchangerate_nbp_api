import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.Scanner;

public final class Util {
   private Util() {
    };

    public enum RatesType {
        ASK , BID , MID;
    }
    public enum CurrencyType{
        USD , EUR , GBP , CHF  ;
    }


    public static String getLinkForExchangeRates(int howManyDaysMinusLocalDate, RatesType ratesType){

        if(ratesType.equals(RatesType.valueOf("ASK")) || ratesType.equals(RatesType.valueOf("BID"))){
            return "http://api.nbp.pl/api/exchangerates/tables/C/" + LocalDate.now().minusDays(howManyDaysMinusLocalDate) + "/?format=json";
        } else {
            return "http://api.nbp.pl/api/exchangerates/tables/A/"  + LocalDate.now().minusDays(howManyDaysMinusLocalDate) + "/?format=json";
        }

    }

    public static ExchangeRateII[] getExchangeRatesFromLink(int howManyDaysMinusLocalDate, RatesType ratesType) throws IOException {
        String link = getLinkForExchangeRates( howManyDaysMinusLocalDate , ratesType);
        URL url = new URL(link);
        URLConnection connection = url.openConnection();
        connection.connect();

            try{
                InputStream is = connection.getInputStream();
                Scanner scanner = new Scanner(is);
                String allOfRate = scanner.nextLine();
                Gson gson1 = new Gson() ;
                is.close();
                return gson1.fromJson(allOfRate, ExchangeRateII[].class);

            } catch (FileNotFoundException e) {
               return getExchangeRatesFromLink(howManyDaysMinusLocalDate + 1, ratesType);

            }

    }


    public static void printProfit(double[] profit, double howMuchIInvested) throws IOException {
        CurrencyType[] ratesName = {CurrencyType.valueOf("USD") , CurrencyType.valueOf("EUR"), CurrencyType.valueOf("GBP"), CurrencyType.valueOf("CHF")};

        for(int i = 0; i < 4; i++ ){
            System.out.println(
                    ratesName[i] + " : " +
                    " Zysk z zainwestownia " + howMuchIInvested + " zł w daną walutę miesiąc temu i sprzedaży jej dzisiaj " +
                    profit[i] + " zł.");
        }
    }



}
