import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExchangeRateII {


    private JsonArray rates;
    private String code;
    private double mid;
    private double ask;
    private double bid;


   public static Map<Util.CurrencyType, Double> getActualExchangeRate(int howManyDaysMinusLocalDate, Util.RatesType ratesType) throws IOException {

       Gson gson = new Gson();


       Map<Util.CurrencyType , Double> actualEchangeRates = new HashMap<>();

       switch (ratesType){
           case ASK:
               JsonArray ask = Util.getExchangeRatesFromLink(howManyDaysMinusLocalDate, Util.RatesType.valueOf("ASK"))[0].rates;
               for (int i = 0; i < ask.size(); i++ ){
                   switch (gson.fromJson(ask.get(i), ExchangeRateII.class).code){
                       case "USD": actualEchangeRates.put(Util.CurrencyType.valueOf("USD"), gson.fromJson(ask.get(i), ExchangeRateII.class).ask);
                       break;
                       case "EUR": actualEchangeRates.put(Util.CurrencyType.valueOf("EUR"), gson.fromJson(ask.get(i), ExchangeRateII.class).ask);
                       break;
                       case "GBP": actualEchangeRates.put(Util.CurrencyType.valueOf("GBP"), gson.fromJson(ask.get(i), ExchangeRateII.class).ask);
                       break;
                       case "CHF": actualEchangeRates.put(Util.CurrencyType.valueOf("CHF"), gson.fromJson(ask.get(i), ExchangeRateII.class).ask);
                       break;
                   }
               }
           case BID:
               JsonArray bid = Util.getExchangeRatesFromLink(howManyDaysMinusLocalDate, Util.RatesType.valueOf("BID"))[0].rates;
               for (int i = 0; i < bid.size(); i++ ){
                   switch (gson.fromJson(bid.get(i), ExchangeRateII.class).code){
                       case "USD": actualEchangeRates.put(Util.CurrencyType.valueOf("USD"), gson.fromJson(bid.get(i), ExchangeRateII.class).bid);
                       break;
                       case "EUR": actualEchangeRates.put(Util.CurrencyType.valueOf("EUR"), gson.fromJson(bid.get(i), ExchangeRateII.class).bid);
                       break;
                       case "GBP": actualEchangeRates.put(Util.CurrencyType.valueOf("GBP"), gson.fromJson(bid.get(i), ExchangeRateII.class).bid);
                       break;
                       case "CHF": actualEchangeRates.put(Util.CurrencyType.valueOf("CHF"), gson.fromJson(bid.get(i), ExchangeRateII.class).bid);
                       break;
                   }
               }
           case MID:
               JsonArray mid = Util.getExchangeRatesFromLink(howManyDaysMinusLocalDate, Util.RatesType.valueOf("MID"))[0].rates;
               for (int i = 0; i < mid.size(); i++ ){
                   switch (gson.fromJson(mid.get(i), ExchangeRateII.class).code){
                       case "USD": actualEchangeRates.put(Util.CurrencyType.valueOf("USD"), gson.fromJson(mid.get(i), ExchangeRateII.class).mid);
                       case "EUR": actualEchangeRates.put(Util.CurrencyType.valueOf("EUR"), gson.fromJson(mid.get(i), ExchangeRateII.class).mid);
                       case "GBP": actualEchangeRates.put(Util.CurrencyType.valueOf("GBP"), gson.fromJson(mid.get(i), ExchangeRateII.class).mid);
                       case "CHF": actualEchangeRates.put(Util.CurrencyType.valueOf("CHF"), gson.fromJson(mid.get(i), ExchangeRateII.class).mid);
                   }
               }
       }


         return actualEchangeRates; }



   public static double[] howMuchIEarnedIfBoughtMonthAgoAndSellNow(double howMuchIInvested) throws IOException {
       Object[] asks = getActualExchangeRate(30, Util.RatesType.valueOf("ASK")).values().toArray();
       Object[] bids = getActualExchangeRate(0, Util.RatesType.valueOf("BID")).values().toArray();

       double[] profit = new double[4];
       for(int i = 0 ; i< profit.length ; i++){
           profit[i] =((Math.floor(( (double)bids[i] - (double)asks[i]) *10000 )) * howMuchIInvested) /10000;

       }
       return profit;

   }


    public static void main(String[] args) throws IOException {

        Util.printProfit(howMuchIEarnedIfBoughtMonthAgoAndSellNow(10000),10000);

    }


}
